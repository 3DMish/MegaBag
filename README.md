# MegaBag - Addon for Blender

MegaBag - it's a mega pack of different stuff for Blender<br>

This repository is a save mirror from "[Blender Projects](https://projects.blender.org/3DMish/MegaBag)"

## Links
- Blender Projects: https://projects.blender.org/3DMish/MegaBag

## Download
- [X] [MegaBag v0.4.0-Alpha](https://gitlab.com/3DMish/MegaBag/-/raw/master/releases/megabag-0-4-0-aplha.zip?inline=false)
- [ ] [MegaBag v0.2.8-Alpha](https://gitlab.com/3DMish/MegaBag/-/raw/master/releases/megabag-0-2-8-alpha.zip?inline=false)
- [ ] [MegaBag v0.2.0-Alpha](https://gitlab.com/3DMish/MegaBag/-/raw/master/releases/megabag-0-2-0-alpha.zip?inline=false)
- [ ] [MegaBag v0.1.0-Alpha](https://gitlab.com/3DMish/MegaBag/-/raw/master/releases/megabag-0-1-0-alpha.zip?inline=false)

## License
GNU General Public License
